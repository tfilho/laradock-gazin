# Laradock Gazin

## About

Laradock Gazin is a full PHP development environment for Docker.

It supports a variety of common services, all pre-configured to provide a ready PHP development environment.

## Requirements
* docker
* docker-compose

## How to Install
### Clone Laradock inside your PHP project:

        git clone https://tfilho@bitbucket.org/tfilho/laradock-gazin.git

### Enter the laradock-gazin folder and copy .env.example to .env.

        cp .env.example .env

### Run your containers and leave it running (The first time this may take a few minutes)

   Linux

        ./gazin-env-docker.sh

   Windows

        docker-compose up nginx mysql php-fpm phpmyadmin


## Ok, So far so good. Now, clone the developer crud project alongside the laradock-gazin project

Your folder structure should look like this:

        * /home/user/worskpace/
            * laradock-gazin
            * developer-crud

## So, open another terminal and clone the developer-crud project

        git clone https://tfilho@bitbucket.org/tfilho/developer-crud.git

### Access the laradock-gazin project and Run the workspace container
Linux

        ./gazin-run-dev-workspace.sh

Windows

        docker-compose exec --user=laradock workspace bash

## You will see something like this

        laradock@b51d665e71ab:/var/www$
        

## Inside the workspace container, install the dependencies

        laradock@b51d665e71ab:/var/www$ composer install

## Create the Enviroment
    
        laradock@b51d665e71ab:/var/www$ cp .env.example .env

## Create a security key for the application

        laradock@b51d665e71ab:/var/www$ php artisan key:generate

## Run the Migrations

        laradock@b51d665e71ab:/var/www$ php artisan migrate:fresh

## Run the Seeders

        laradock@b51d665e71ab:/var/www$ php artisan db:seed

## Open your browser and access the link

        http://localhost

## Tests
        
        laradock@b51d665e71ab:/var/www$ php artisan test

## Issues

If you get this message:

PermissionError: [Errno 13] Permission denied: 'laradock-gazin/nginx/ssl/default.key'       

Try this:

        cd nginx/ssl/
        rm default.crt default.csr default.key
        docker-compose down

And run docker-compose again
